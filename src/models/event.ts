import {IEventDoc} from './base/event';
import Event from './ajax/event';

export {
  Event,
  IEventDoc,
};
export default Event;
