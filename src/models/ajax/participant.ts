import { axios } from './config';
import Base, { IParticipantDoc } from '../base/participant';


export class Participant extends Base {
  static async getParticipant(eventId: string, bibNumber: number) {
    if (typeof bibNumber !== 'number') { throw new Error(`getParticipant expects bibNumber to be a number, but it was of type ${typeof bibNumber}`); }
    let doc: IParticipantDoc | null = null;
    try {
      const resp = await axios.get(`/api/v1/events/${eventId}/participants/${bibNumber}`);
      doc = resp.data;
    } catch (e) {
      console.error(e);
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }

  static async getParticipants(eventId: string, ids?: string[]) {
    const docs: Participant[] = [];
    try {
      // TODO filter by ids
      const resp = await axios.get(`/api/v1/events/${eventId}/participants`);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e);
    }
    return docs;
  }

  static async delete(participant: Participant) {
    await axios.delete(`/api/v1/participant/${participant.id}`);
  }

  async save() {
    if (this.id) {
      await axios.put<Participant>(`/api/v1/participant/${this.id}`, dbConverter.toDB(this));
    } else {
      const resp = await axios.post<Participant>(`/api/v1/participant`, dbConverter.toDB(this));
      this.id = resp.data.id;
    }
  }
}
export default Participant;

// DB data converter
const dbConverter = {
  toDB(x: Participant) {
    return x;
  },
  fromDB(doc: IParticipantDoc) {
    const raceEventId = String(doc.eventId);
    return new Participant(raceEventId, Object.assign({}, doc));
  },
};
