import Vue from 'vue';
import Vuex, { ActionContext } from 'vuex';

import modules from './loader';

export interface RootState {
  settings: import('./settings/module').SettingsState;
}
export type RTActionContext<T> = ActionContext<T, RootState>;

Vue.use(Vuex);

const store = new Vuex.Store<RootState>({
  state: {
  } as any,
  modules,
  strict: process.env.NODE_ENV !== 'production',
});

const ajaxConfig = require('@/models/ajax/config') as typeof import ('@/models/ajax/config'); // eslint-disable-line @typescript-eslint/no-var-requires
ajaxConfig.watchVuexAndUpdate(store);

export default store;

if (isDev) {
  (window as any).store = store;
}
