import {IParticipantDoc} from './base/participant';
import Participant from './ajax/participant';

export {
  Participant,
  IParticipantDoc,
};
export default Participant;
