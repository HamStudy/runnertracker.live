export const dbName = <const>'races';

// tslint:disable max-classes-per-file
abstract class RaceDoc {
  id!: string;
  eventId!: string;
  name!: string;
  stations!: string[];
}
export type IRaceDoc = RaceDoc;
export class Race extends RaceDoc {
  static async getRace(eventId: string, id: string): Promise<Race | null> { return null; }
  static async getRaces(eventId: string): Promise<Race[]> { return []; }
  static async delete(race: Race): Promise<void> { return; }

  constructor(
    eventId: string,
    obj: Partial<Race> & Pick<Race, 'name'>,
  ) {
    super();
    Object.assign(this, obj, {eventId});
  }
  async save(): Promise<void> { return; }
}
export default Race;
