import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Event from '@/models/event';
import routes, { ROUTES } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach(async (routeTo, routeFrom, next) => {
  if (routeTo.name === 'catchAll') {
    const events = await Event.getEvents();
    const ongoingEvents = events.filter(e => {
      if (!e.startDate) { return false; }
      if (e.startDate && e.startDate.toLocaleDateString() === new Date().toLocaleDateString()) { return true; }
      if (!e.endDate) { return false; }
      if ((e.startDate.getTime() - e.startDate.getTime() % 864E5) < Date.now() && Date.now() < ((e.endDate.getTime() - e.endDate.getTime() % 864E5) + 864E5)) { return true; }
      return false;
    });
    if (ongoingEvents.length === 1) {
      next({name: ROUTES.search.name, params: {eventId: ongoingEvents[0].id}});
    } else {
      next({name: ROUTES.events.name});
    }
  } else {
    next();
  }
});

export default router;
