import VueI18next from '@panter/vue-i18next';
import Vue from 'vue';
import { i18next } from './locales/i18next';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import './filters';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@fortawesome/fontawesome-free/css/all.css';
import './main.scss';

import App from './App.vue';

Vue.config.productionTip = false;
i18next.init();
Vue.use(VueI18next);

new Vue({
  i18n: new VueI18next(i18next),
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
