// Register each file as a corresponding Vuex module. Module nesting
// will mirror [sub-]directory hierarchy and modules are namespaced
// as the camelCase equivalent of their file name.

interface Modules {
    modules: Module;
  }

interface Module {
    [name: string]: Modules;
  }

/** https://webpack.js.org/guides/dependency-management/#require-context */
const requireModule = (require as any).context(
    // Search for files in the current directory
    '.',
    // Search for files in subdirectories
    true,
    // Include any .ts files that are not unit tests
    /\bmodule.ts$/,
  );
const root: Modules = { modules: {} };

/** Recursively get the namespace of the module, even if nested */
function getNamespace(subtree: Modules, path: string[]): Modules {
    if (path.length === 1) { return subtree; }

    const namespace = path.shift() as string;
    // Set a placeholder modules obj but overwrite with any previous modules
    subtree.modules[namespace] = {
      // modules: {},
      ...subtree.modules[namespace],
    };
    return getNamespace(subtree.modules[namespace], path);
  }

requireModule.keys().forEach((fileName: string) => {
    // Skip this file, as it's not a module
    if (fileName === './loader.ts') { return; }

    // Get the module path as an array
    const modulePath = fileName
      // Remove the "./" from the beginning
      .replace(/^\.\//, '')
      // Remove the file extension from the end
      .replace(/\/module\.\w+$/, '')
      // Split nested modules into an array path
      .split(/\//)
    ;

    // Get the modules object for the current path
    const { modules } = getNamespace(root, modulePath);


    // Add the module to our modules object
    const moduleName = modulePath.pop() as string;
    modules[moduleName] = {
      ...modules[moduleName], // don't overwrite submodules that might have been processed first
      ...(requireModule(fileName).default),
      // Modules are namespaced by default
      namespaced: true,
    };
  });

export default root.modules;
