import { axios } from './config';
import Base, { IEventDoc } from '../base/event';


export class Event extends Base {
  static async getEvent(id: string): Promise<Event | null> {
    if (!id) { return null; }
    let doc: IEventDoc | null = null;
    try {
      const resp = await axios.get(`/api/v1/events/${id}`);
      doc = resp.data;
    } catch (e) {
      console.error(e);
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getEvents(): Promise<Event[]> {
    const docs: Event[] = [];
    try {
      const resp = await axios.get(`/api/v1/events`);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e);
    }
    return docs;
  }
  static async delete(event: Event) {
    await axios.delete(`/api/v1/events/${event.id}`);
  }

  async save() {
    if (this.id) {
      await axios.put<IEventDoc>(`/api/v1/events/${this.id}`, dbConverter.toDB(this));
    } else {
      const resp = await axios.post<IEventDoc>(`/api/v1/events`, dbConverter.toDB(this));
      this.id = resp.data.id;
    }
  }
}
export default Event;

// DB data converter
const dbConverter = {
  toDB(x: Event) {
    const doc: IEventDoc = {
      id: x.id,
      name: x.name,
      startDate: x.startDate,
      endDate: x.endDate,
      slug: x.slug,
    };
    return doc;
  },
  fromDB(doc: IEventDoc) {
    const startDate = doc.startDate ? new Date(doc.startDate) : (doc as any).date ? new Date((doc as any).date) : (void 0);
    const endDate = doc.endDate ? new Date(doc.endDate) : (void 0);
    const id = doc.id;
    const slug = doc.slug;
    return new Event({id, name: doc.name, startDate, endDate, slug});
  },
};
