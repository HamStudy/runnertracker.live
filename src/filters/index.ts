import Vue from 'vue';
import { formatDate } from '@/lib/formatDate';

Vue.filter('date', (date: Date, format?: string, defaultVal = '') => {
  if (!date) { return defaultVal; }
  return formatDate(date, format || 'MM/DD HH:mm');
});

Vue.filter('time', (x: Date | null) => {
  if (!x) { return '———'; }
  return `${('00' + x.getHours()).substr(-2)}:${('00' + x.getMinutes()).substr(-2)}`;
});