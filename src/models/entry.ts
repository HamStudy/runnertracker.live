import {IEntryDoc} from './base/entry';
import Entry from './ajax/entry';

export {
  Entry,
  IEntryDoc,
};
export default Entry;
