import { axios } from './config';
import Base, { IStationDoc } from '../base/station';


export class Station extends Base {
  // This is used to verify that the station id we have belongs to this event, if not it'll be null
  static async getStation(obj: {eventId: string; stationId?: string}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation(obj: {eventId: string; raceId: string | undefined; stationNumber: number}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation({eventId, raceId, stationId, stationNumber}: Partial<{eventId: string; raceId: string; stationId: string; stationNumber: number}>): Promise<Station | null> {
    let doc: IStationDoc | undefined;
    try {
      let url = '';
      if (stationId) {
        url = `/api/v1/station/${stationId}`;
        if (eventId) {
          url += `?event=${eventId}`;
        }
      } else if (eventId && stationNumber) {
        url = `/api/v1/events/${eventId}/stations/${stationNumber}`;
        if (raceId) {
          url += `?race=${raceId}`;
        }
      }
      if (!url) { return null; }
      const resp = await axios.get(url);
      doc = resp.data;
    } catch (e) {
      console.error(e);
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }

  static async getStations(eventId: string, raceId: string | undefined) {
    const docs: Station[] = [];
    try {
      let url = `/api/v1/events/${eventId}/stations`;
      if (raceId) {
        url += `?raceId=${raceId}`;
      }
      const resp = await axios.get(url);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e);
    }
    return docs;
  }

  static async delete(station: Station) {
    await axios.delete(`/api/v1/station/${station.id}`);
  }

  async save() {
    if (this.id) {
      await axios.put<Station>(`/api/v1/station/${this.id}`, dbConverter.toDB(this));
    } else {
      const resp = await axios.post<Station>(`/api/v1/station`, dbConverter.toDB(this));
      this.id = resp.data.id;
    }
  }
}
export default Station;

// DB data converter
const dbConverter = {
  toDB(x: Station) {
    return {
      id: x.id,
      eventId: x.eventId,
      name: x.name,
      distance: x.distance,
      stationNumber: x.stationNumber,
    };
  },
  fromDB(doc: IStationDoc) {
    const raceEventId = String(doc.eventId);
    return new Station(raceEventId, Object.assign({}, doc));
  },
};
