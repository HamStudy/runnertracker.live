import {IStationDoc} from './base/station';
import Station from './ajax/station';

export {
  Station,
  IStationDoc,
};
export default Station;
