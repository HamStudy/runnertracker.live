import { i18next } from '@/locales/i18next';

const replacementMap = {
  YYYY: (date: Date) => ('000' + date.getFullYear()).substr(-4),
  YY: (date: Date) => ('000' + date.getFullYear()).substr(-2),
  MMMM: (date: Date) => i18next.t(`months.${date.getMonth()}`),
  MMM: (date: Date) => i18next.t(`months.${date.getMonth()}`, {context: 'short'}),
  MM: (date: Date) => ('0' + (date.getMonth()+1)).substr(-2),
  M: (date: Date) => String(date.getMonth()+1),
  DD: (date: Date) => ('0' + date.getDate()).substr(-2),
  D: (date: Date) => ('' + date.getDate()).substr(-2),
  HH: (date: Date) => ('0' + date.getHours()).substr(-2),
  mm: (date: Date) => ('0' + date.getMinutes()).substr(-2),
  ss: (date: Date) => ('0' + date.getSeconds()).substr(-2),
  dddd: (date: Date) => i18next.t(`days.${date.getDay()}`),
  ddd: (date: Date) => i18next.t(`days.${date.getDay()}`, {context: 'short'}),
};

const interpolationMap = [
  <const>['YYYY', '{{1}}'],
  <const>['YY', '{{2}}'],
  <const>['MMMM', '{{3}}'],
  <const>['MMM', '{{4}}'],
  <const>['MM', '{{5}}'],
  <const>['M', '{{6}}'],
  <const>['DD', '{{7}}'],
  <const>['D', '{{8}}'],
  <const>['HH', '{{9}}'],
  <const>['mm', '{{10}}'],
  <const>['ss', '{{11}}'],
  <const>['dddd', '{{12}}'],
  <const>['ddd', '{{13}}'],
 ];

export function formatDate(date: Date, format?: string) {
  if (!format) { return date.toString(); }
  let str = format;
  for (const [key,interpolated] of interpolationMap) {
    str = str.replace(key, interpolated);
  }
  for (const [key,interpolated] of interpolationMap) {
    str = str.replace(interpolated, replacementMap[key](date));
  }
  return str;
}
export default formatDate;
