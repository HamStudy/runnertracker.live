import Vue from 'vue';
import { } from 'vuetify'; // This is so typescript can find 'vuetify/lib'
import Vuetify from 'vuetify/lib';
import scssVariables from '@/variables.scss';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    dark: false,
    themes: {
      light: {
        hamstudyBlue: scssVariables.hamstudyBlue,
        primary: scssVariables.primary,
        secondary: scssVariables.secondary,
        accent: scssVariables.accent,
        error: scssVariables.error,
        info: scssVariables.info,
        success: scssVariables.success,
        warning: scssVariables.warning,
      },
      dark: {
        hamstudyBlue: scssVariables.hamstudyBlue,
        primary: scssVariables.primary,
        secondary: scssVariables.secondary,
        accent: scssVariables.accent,
        error: scssVariables.error,
        info: scssVariables.info,
        success: scssVariables.success,
        warning: scssVariables.warning,
      },
    },
  },
  icons: {
    iconfont: 'fa',
  },
});
