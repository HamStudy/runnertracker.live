'use strict';

module.exports = {
  publicPath: '/',
  /**
   * @param config {import('webpack').Configuration}
   */
  chainWebpack: config => {
    config.plugin('html')
      .tap(args => {
        args[0] = {
          ...(args[0] || {}),
          'title': 'TRacer',
        };
        return args;
      })
    ;
    config.plugin('define')
      .tap(args => {
        args[0] = {
          ...(args[0] || {}),
          'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
          'isDev': process.env.NODE_ENV === 'development',
        };
        return args;
      })
    ;
  },
  transpileDependencies: [
    'vuetify',
  ],
};
