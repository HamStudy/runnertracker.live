import { RouteConfig } from 'vue-router';

export const ROUTES = {
  events: {
    path: '/',
    name: 'Events',
    component: () => import(/* webpackChunkName: "events" */ '../views/Events.vue'),
  } as RouteConfig,
  participants: {
    path: '/:eventSlugOrId/:bibNumber',
    name: 'Participant',
    component: () => import(/* webpackChunkName: "participant" */ '../views/Participant.vue'),
  } as RouteConfig,
  search: {
    path: '/:eventSlugOrId',
    name: 'Search',
    component: () => import(/* webpackChunkName: "participant" */ '../views/Search.vue'),
  } as RouteConfig,
  catchAll: {
    path: '*',
    name: 'catchAll',
  } as RouteConfig,
};

export default [
  ROUTES.events,
  ROUTES.participants,
  ROUTES.search,
  ROUTES.catchAll,
];