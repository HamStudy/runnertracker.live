import { axios } from './config';
import Base, { IRaceDoc } from '../base/race';


export class Race extends Base {
  static async getRace(eventId: string, id: string): Promise<Race | null> {
    if (!id) { return null; }
    let doc: IRaceDoc | null = null;
    try {
      const resp = await axios.get(`/api/v1/events/${eventId}/races/${id}`);
      doc = resp.data;
    } catch (e) {
      console.error(e);
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getRaces(eventId: string): Promise<Race[]> {
    const docs: Race[] = [];
    try {
      const resp = await axios.get(`/api/v1/events/${eventId}/races`);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e);
    }
    return docs;
  }
  static async delete(race: Race) {
    await axios.delete(`/api/v1/events/${race.eventId}/races/${race.id}`);
  }

  async save() {
    if (this.id) {
      await axios.put<IRaceDoc>(`/api/v1/events/${this.id}`, dbConverter.toDB(this));
    } else {
      const resp = await axios.post<IRaceDoc>(`/api/v1/events`, dbConverter.toDB(this));
      this.id = resp.data.id;
    }
  }
}
export default Race;

// DB data converter
const dbConverter = {
  toDB(x: Race) {
    return x;
  },
  fromDB(doc: IRaceDoc) {
    const eventId = String(doc.eventId);
    return new Race(eventId, Object.assign({}, doc));
  },
};
