import pathify, { make } from 'vuex-pathify';
// import store, { RTActionContext } from '@/store';

const PATHIFY_MAPPING = pathify.options.mapping;
pathify.options.mapping = 'simple';

const moduleState = {
  authentication: '',
  serverUri: '',
};
export type SettingsState = typeof moduleState;
// type ActionContext = RTActionContext<SettingsState>;

// GETTERS
const moduleGetters = {};

// MUTATIONS
const moduleMutations = {
  ...make.mutations(moduleState),
};

// ACTIONS
const moduleActions = {
  ...make.actions(moduleState),
};

export default {
  namespaced: true,
  state: moduleState,
  mutations: moduleMutations,
  actions: moduleActions,
  getters: moduleGetters,
};


pathify.options.mapping = PATHIFY_MAPPING;
