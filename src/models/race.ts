import {IRaceDoc} from './base/race';
import Race from './ajax/race';

export {
  Race,
  IRaceDoc,
};
export default Race;
