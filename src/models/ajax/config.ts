import Axios from 'axios';

export const axios = Axios.create();

(window as any).axios = axios;

export function watchVuexAndUpdate(store: typeof import('@/store').default) {
  store.watch(s => s.settings.serverUri, (n, o) => {
    axios.defaults.baseURL = n;
  }, {immediate: true});
  store.watch(s => s.settings.authentication, (n, o) => {
    if (n) {
      axios.defaults.headers.authentication = n;
    } else {
      delete axios.defaults.headers.authentication;
    }
  }, {immediate: true});

  (window as any).login = async (apiToken: string) => {
    const resp = await axios.post('/api/user/login', {apiToken});
    store.dispatch('settings/setAuthentication', `Bearer ${resp.data}`);
  };
}

if (isDev) {
  (window as any).axios = axios;
}
